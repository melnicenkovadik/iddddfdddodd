window.onload = function () {
    wow = new WOW({
        live:         true,
        callback:     function(box) {
            console.log('animation started');
        }
    });
    wow.init();
    window.addEventListener("resize", getResize);
    function getResize(e) {

        console.clear();
        console.log('window reiszed', e.target.innerWidth);
    }

    var image1 = document.querySelector(".change-item");
    var image2 = document.querySelector(".item-to-change");

    function changeDish(e) {
        var rel = image1.src;
        image1.src = image2.src;
        image2.src = rel;
    }

    function show3dModel() {
        var modalBody = document.querySelector(".modal__title");
        modalBody.innerHTML = `<img src=${image1.src}>`

    }

    image1.addEventListener('click', show3dModel)
    image2.addEventListener('click', changeDish)
}

document.addEventListener('DOMContentLoaded', function () {


    var modalButtons = document.querySelectorAll('.js-open-modal'),
        overlay = document.querySelector('.js-overlay-modal'),
        closeButtons = document.querySelectorAll('.js-modal-close');
    modalButtons.forEach(function (item) {
        item.addEventListener('click', function (e) {
            e.preventDefault();
            var modalId = this.getAttribute('data-modal'),
                modalElem = document.querySelector('.modal[data-modal="' + modalId + '"]');
            modalElem.classList.add('active');
            overlay.classList.add('active');
        });
    });
    closeButtons.forEach(function (item) {
        item.addEventListener('click', function (e) {
            var parentModal = this.closest('.modal');
            parentModal.classList.remove('active');
            overlay.classList.remove('active');
        });
    });
    document.body.addEventListener('keyup', function (e) {
        var key = e.keyCode;
        if (key == 27) {
            document.querySelector('.modal.active').classList.remove('active');
            document.querySelector('.overlay').classList.remove('active');
        }
        ;
    }, false);
    overlay.addEventListener('click', function () {
        document.querySelector('.modal.active').classList.remove('active');
        this.classList.remove('active');
    });
});